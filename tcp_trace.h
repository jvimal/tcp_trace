
#undef TRACE_SYSTEM
#define TRACE_SYSTEM vimal

#if !defined(_TRACE_EVENT_VIMAL_H) || defined(TRACE_HEADER_MULTI_READ)
#define _TRACE_EVENT_VIMAL_H

#include <linux/tracepoint.h>

TRACE_EVENT(tcp_rx,

            TP_PROTO(u64 ts, u32 saddr, u32 daddr, u16 sport, u16 dport, u32 snd_nxt, u32 snd_una),

            TP_ARGS(ts, saddr, daddr, sport, dport, snd_nxt, snd_una),

            TP_STRUCT__entry(
                             __field(u64, ts)
                             __field(u32, saddr)
                             __field(u32, daddr)
                             __field(u16, sport)
                             __field(u16, dport)
                             __field(u32, snd_nxt)
                             __field(u32, snd_una)
                             __field(u64, magic)
                             ),

            TP_fast_assign(
                           __entry->ts = ts;
                           __entry->saddr = saddr;
                           __entry->daddr = daddr;
                           __entry->sport = sport;
                           __entry->dport = dport;
                           __entry->snd_nxt = snd_nxt;
                           __entry->snd_una = snd_una;
			   __entry->magic = 0xdeadbeef;
                           ),

            TP_printk("ts: %llu, saddr: %x:%x, daddr: %x:%x, snd_nxt: %u, snd_una: %u",
	    		__entry->ts, __entry->saddr, __entry->sport,
			__entry->daddr, __entry->dport, __entry->snd_nxt, __entry->snd_una)
            );

#endif

#undef TRACE_INCLUDE_PATH
#undef TRACE_INCLUDE_FILE
#define TRACE_INCLUDE_PATH .
#define TRACE_INCLUDE_FILE tcp_trace

#include <trace/define_trace.h>

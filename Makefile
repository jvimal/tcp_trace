
obj-m += tcp_trace.o
CFLAGS_tcp_trace.o := -I$(src)

all:
	make -C /lib/modules/$(shell uname -r)/build M=`pwd`

clean:
	rm -f *.o *.ko *.mod.c modules.* Module.*

